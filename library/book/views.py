from uuid import uuid4

from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.views.generic import View

# from book.filters import BookFilter
# from book.forms import BookForms
from book.models import Book


class BooksListView(View):
    def get(self, request):
        # filter_form = BookFilter(request.GET, request=request)

        return render(request, "books/list.html", {
            "extraHeadContext": {
                "title": "Books List"
            },
            "context": {
                # "situation": request.GET.get("situation"),
                # "titleOfPage": request.GET.get("situation", "").upper(),
                # "filter": filter_form
            }
        })


# class BookCreateView(View):
#     def post(self, request):
#         create_form = BookForms(request.POST, request.FILES)
#         if create_form.is_valid():
#             create_form.save()
#
#             return redirect(
#                 # f"{reverse('books-list')}?situation={request.POST.get('situation')}"
#             )
#
#         return render(request, "books/form.html", {
#             "extraHeadContext": {
#                 "title": "Book Create"
#             },
#             "context": {
#                 # "situation": request.GET.get("situation"),
#                 # "titleOfPage": request.GET.get("situation", "").upper(),
#                 # "form": create_form
#             }
#         })
#
#     def get(self, request):
#         create_form = BookForms(
#             initial={
#                 "book_id": str(uuid4())
#             }
#         )
#
#         return render(request, "books/form.html", {
#             "extraHeadContext": {
#                 "title": "Books Create"
#             },
#             "context": {
#                 # "situation": request.GET.get("situation"),
#                 # "titleOfPage": request.GET.get("situation", "").upper(),
#                 # "form": create_form
#             }
#         })
#
#
# class BookDeleteView(View):
#     def get(self, request, book_id):
#         book = Book.objects.filter(book_id=book_id)
#         book.delete()
#
#         return redirect(
#             # f"{reverse('books-list')}?situation={request.GET.get('situation')}"
#         )
#
#
# class BookDetailView(View):
#     def get(self, request, book_id):
#         book = get_object_or_404(Book, book_id=book_id)
#         detail_form = BookForms(detail=True, initial=book.to_dict())
#
#         return render(request, "books/form.html", {
#             "extraHeadContext": {
#                 "title": "Books Detail"
#             },
#             "context": {
#                 # "situation": request.GET.get("situation"),
#                 # "titleOfPage": car.situation.upper(),
#                 # "mode": 'detail',
#                 # "form": detail_form
#             }
#         })
