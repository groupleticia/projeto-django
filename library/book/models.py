from django.db import models
import uuid

class Book(models.Model):

    book_id = models.UUIDField(
        primary_key=True,
        auto_created=True,
        default=str(uuid.uuid4())
    )

    name = models.CharField(max_length=100)
    category = models.CharField(max_length=100)
    release_year = models.IntegerField()
    edition = models.IntegerField()
    qtd_page = models.IntegerField()

    photo_book = models.ImageField(upload_to='photos/')

    def to_dict(self):
        return {
            "book_id": str(self.book_id),
            "name": self.name,
            "category": self.category,
            "release_year": self.release_year,
            "edition": self.edition,
            "qtd_page": self.qtd_page,
            "photo_book": self.photo_book,
        }

    def __str__(self):
        return f'Book: {self.book_id}'