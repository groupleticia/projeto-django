from django.urls import path

from book.views import BooksListView

urlpatterns = [
    path('', BooksListView.as_view(), name='books-list'),
    # path('register/', BookCreateView.as_view(), name='books-create'),
    # path('<str:book_id>/', BookDeleteView.as_view(), name='book-delete'),
    # path('<str:book_id>/detail/', BookDetailView.as_view(), name='book-detail'),
]